'''

0 - 5->2->5 =[5,2]
1 - [2,3] = 2
not id - 0

logs: [[0,5], [1,2], [0,2], [0,5], [1,3]]


User Id = 1 → 2

User Id = 0 → 2

User Id = 4 → 0

Given the logs for users' actions on Facebook, find a user’s active minutes spent.
The logs are represented by a 2D integer array logs where each logs[i] = [UserIDi, timei] indicates that the user with UserIDi performed an action at the minute timei.

Constraints:

1 <= logs.length <= 10^4

0 <= UserIDi <= 10^9

1 <= timei <= 10^5


'''

#def findUserActiveMinutes(userId):

def UserSession(logs):

    logDict = {}
    #count = 0
    for log in logs:
        #USerSet = set()
        logDict[log[0]] = set()


    for log in logs:

        #timeSet = set()
        
        uId,time = log[0],log[1]
        #logDict[uId]=set()
        
        
        
        #unique values of time
        #timeSet.add(time)
        #if uId not in log:
            #print(0)
        logDict[uId].add(time)
        
    for user in logDict:
        print("User Id = ", user, "->", len(logDict[user]))
        

logs=[[0,5], [1,2], [0,2], [0,5], [1,3]]
UserSession(logs)
        
''' 
 0 
 set = 5,2
 0,5 
 
 logDict[0] = (5,2)
 
 
 1
 1,2
 lD = (2,3)
'''
            



